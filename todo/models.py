from django.db import models
from django.utils.timezone import now


class Task (models.Model):
    creation_time = models.DateTimeField('creation time', default=now,)
    content = models.CharField(max_length=200)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return self.content
